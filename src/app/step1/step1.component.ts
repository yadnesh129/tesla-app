import { Component, OnInit } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { Model } from '../shared/models/Model';
import { Color } from '../shared/models/Color';
import { SelectedCar } from '../shared/models/SelectedCar';
import { CommonModule } from '@angular/common';

import { SharedService } from '../shared/services/shared.service';
import { HttpService } from '../shared/services/http.service';

@Component({
  selector: 'app-step1',
  standalone: true,
  imports: [FormsModule,CommonModule],
  templateUrl: './step1.component.html',
  styleUrl: './step1.component.scss'
})
export class Step1Component implements OnInit {
  selectedModel?:Model;
  selectedCar:SelectedCar=new SelectedCar();
  vehicleModels: Array<Model> =[];
  selectedColor?:Color;

  constructor(private sharedService:SharedService,private httpSerivce:HttpService){}
 
  ngOnInit(){
    this.sharedService.selectedCarObservable.subscribe(
      (selectedCar:SelectedCar) =>
      { 
        this.selectedCar=selectedCar;
        this.httpSerivce.getCarModels().subscribe(
          data =>
          {
            this.vehicleModels=data;
            this.selectedModel=this.vehicleModels.find(x=> x.code == this.selectedCar.model?.code);
            this.selectedColor=this.selectedModel?.colors.find(x=>x.code == this.selectedCar.color?.code); 
          }
        );
      }
    );
  }

  onColorChange(){
    this.selectedCar.color=this.selectedColor;
    this.sharedService.selectedCar(this.selectedCar);
  }

  onModelChange(){
    //set color back to undefined.
    this.selectedColor=undefined;
    this.selectedCar=new SelectedCar();
    this.selectedCar.model=this.selectedModel;
    this.sharedService.selectedCar(this.selectedCar);
  }

 

  

}
