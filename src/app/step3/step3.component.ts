import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { SelectedCar } from '../shared/models/SelectedCar';
import { SharedService } from '../shared/services/shared.service';
import { CustomCurrencyPipe } from "../shared/pipes/custom-currency.pipe";

@Component({
    selector: 'app-step3',
    standalone: true,
    templateUrl: './step3.component.html',
    styleUrl: './step3.component.scss',
    imports: [CommonModule, CustomCurrencyPipe]
})
export class Step3Component implements OnInit{
  selectedVehicle:SelectedCar=new SelectedCar();

  constructor(private sharedService:SharedService){}

  ngOnInit(): void {
    this.sharedService.selectedCarObservable.subscribe(selectedCar=>
      this.selectedVehicle=selectedCar
    );
  }

}
