import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customCurrency',
  standalone: true
})
export class CustomCurrencyPipe implements PipeTransform {

  transform(value:string | null): string {
    if(value == null)
      return "";
    else
      return '$'+value;
  }

}
