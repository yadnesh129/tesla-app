import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SelectedCar } from '../shared/models/SelectedCar';
import { Config } from '../shared/models/Config';
import { HttpService } from '../shared/services/http.service';
import { SharedService } from '../shared/services/shared.service';
import { Option } from '../shared/models/Option';
import { CustomCurrencyPipe } from '../shared/pipes/custom-currency.pipe';

@Component({
  selector: 'app-step2',
  standalone: true,
  imports: [CommonModule,FormsModule,CustomCurrencyPipe],
  templateUrl: './step2.component.html',
  styleUrl: './step2.component.scss'
})
export class Step2Component implements OnInit{
  
  selectedConfig?:Config;
  vehicleOptions?:Option;
  towHitchChecked:boolean=false;
  yokeChecked:boolean=false;

  selectedCar:SelectedCar=new SelectedCar();

  constructor(
    private httpService:HttpService,
    private sharedService:SharedService){}


  ngOnInit(): void {
    this.sharedService.selectedCarObservable.subscribe((selectedCar:SelectedCar)=>{
      console.log(JSON.stringify(selectedCar));

      this.selectedCar=selectedCar;

      this.httpService.getCarOptions(this.selectedCar.model?.code!).subscribe((options:Option)=>{
        this.vehicleOptions=options;
        this.selectedConfig=this.vehicleOptions?.configs.find(a=>a.id == this.selectedCar.config?.id);
        this.yokeChecked=this.selectedCar.yoke;
        this.towHitchChecked=this.selectedCar.tow
      })
    }
    )
  }

  onTowHitch(){
    console.log("Tow Hitch checked -> "+this.towHitchChecked)
    this.selectedCar.tow = this.towHitchChecked;
    this.sharedService.selectedCar(this.selectedCar);
   
  }

  onYokeChanged(){
    console.log("Yoke checked -> "+this.yokeChecked)
    this.selectedCar.yoke=this.yokeChecked;
    this.sharedService.selectedCar(this.selectedCar);
    
    
  }
  onSelecConfigChanged(){
    this.selectedCar.config=this.selectedConfig;
    this.sharedService.selectedCar(this.selectedCar);
    
  }

  





}
