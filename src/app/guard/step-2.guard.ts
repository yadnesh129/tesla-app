import { inject } from "@angular/core";
import { CanActivateFn, Router } from "@angular/router";
import { SharedService } from "../shared/services/shared.service";
import { SelectedCar } from "../shared/models/SelectedCar";


export function step2Guard():CanActivateFn{

    return ()=>{
        let sharedService= inject(SharedService);
        let router= inject(Router);
       
        let isActive:boolean=false;
        sharedService.selectedCarObservable.subscribe((selectedCar:SelectedCar)=>
            isActive = !selectedCar.colorAndModelNotSelected()
        );

        if(!isActive){
            router.navigateByUrl('/step1');
        }
        return isActive;
    }
}