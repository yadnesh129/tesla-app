import {Component} from '@angular/core';
import {AsyncPipe, CommonModule, JsonPipe} from '@angular/common';
import { RouterLink, RouterOutlet } from '@angular/router';
import { SelectedCar } from './shared/models/SelectedCar';
import { SharedService } from './shared/services/shared.service';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [AsyncPipe, JsonPipe,RouterLink,RouterOutlet,CommonModule],
  templateUrl: './app.component.html',
})
export class AppComponent {
  step3Disabled: boolean =true;
  step2Disabled: boolean =true;
  selectedCar:SelectedCar=new SelectedCar();
  imgUrl?:string;
  

  constructor(
    private sharedService:SharedService
  ){}

  ngOnInit(): void {
    this.sharedService.selectedCarObservable.subscribe((selectedCar:SelectedCar) => { 
        this.selectedCar=selectedCar;
        this.step2Disabled = this.selectedCar.colorAndModelNotSelected();
        this.step3Disabled = this.selectedCar.configNotSelected();
        // console.log(this.selectedCar.model?.code+" "+ this.selectedCar.color?.code);
        this.imgUrl="assets/images/"+this.selectedCar.model?.code+"/"+this.selectedCar.color?.code+".jpg";
      }
    ); 
    
  }

}
